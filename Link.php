<?php 
namespace app\admin\controller;
use app\admin\model\Link as LinkModel;

class Link extends Common
{
    public function lst()
    {
        #排序
        if(request()->isPost()){
            $link = new LinkModel();
            $sorts = input('post.');
            foreach ($sorts as $k => $v) {
                $link->update(['id'=>$k,'sort'=>$v]);
            }
            $this->success('更新排序成功',url('lst'));
            return;
        }
        #列表页
        $linkres = LinkModel::order('sort desc')->paginate(2);
        $this->assign('linkres',$linkres);
        return view();
    }


    public function add()
    {
        if(request()->isPost()){
            $data = input('post.');
            // dump($data);die;
            $validate = \think\Loader::validate('Link');
            if(!$validate->scene('add')->check($data)){
                $this->error($validate->getError());
            }
            $add = LinkModel::create($data);
            if($add){
                $this->success('添加链接成功',url('lst'));
            }else{
                $this->error('添加友链失败');
            }

            return;
        }
        return view();
    }

    public function edit()
    {   
        if(request()->isPost()){
            $link = new LinkModel();
            $data = input('post.');
            $validate = \think\Loader::validate('Link');
            if(!$validate->scene('edit')->check($data)){
                $this->error($validate->getError());
            }
            $save = $link->update($data);
            if($save !== false){
                $this->success('修改成功',url('lst'));
            }else{
                $this->error('修改失败');
            }
            return;
        }
        $links = LinkModel::find(input('id'));
        $this->assign('links',$links);
        return view();
    }


    public function del()
    {
        $del = LinkModel::destroy(input('id'));
        if($del){
            $this->success('删除链接成功',url('lst'));
        }else{
            $this->error('删除链接失败!');
        }

    }



}