<?php 
namespace app\admin\controller;
use think\Controller;
use app\admin\model\Admin as AdminModel;
class Login extends Controller
{
    public function index()
    {   
        if(request()->isPost()){
            $admin = new AdminModel();
            $loginnum  = $admin->login(input('post.'));
            if($loginnum==1){
                $this->error('用户不存在');
            }
            if($loginnum==2){
                $this->success('登录成功',url('index/index'));
            }
            if($loginnum==3){
                $this->error('密码错误');
            }

            return;
        }

        return view('login');
    }


}
